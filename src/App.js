import "./App.css";
import HelloWorld from "./components/HelloWorlds/HelloWorld";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HelloWorld></HelloWorld>
      </header>
    </div>
  );
}

export default App;
